//Příklad 1
// - mame, zoznam zvierat v zoo, ich ostrovatel, hodina krmenia (kludne dajme aj inu opateru)
let zoo = [
    {animal:"unicorn", keeper:"Jan",feeding:22},
    {animal:"unicorn15", keeper:"Eva",feeding:11},
    {animal:"unicorn3", keeper:"Jan",feeding:12},
    {animal:"unicorn7", keeper:"Jana",feeding:13},
    {animal:"unicorn4", keeper:"Jan",feeding:15},
    {animal:"unicorn8", keeper:"Jana",feeding:15},
    {animal:"unicorn5", keeper:"Jan",feeding:16},

]


// vytvorme funkciu, ktora vrati rozvrh krmenia pre osetrovatela
// - napr. funkcia getFeedingTimetable("Jan") vrátí:
// [
// {​​​​​​​animal: "lion", time: 14}​​​​​​​,
// {​​​​​​​animal: "tiger", time: 15}​​​​​​​,
// {​​​​​​​animal: "wolf", time: 16}​​​​​​​
// ]

function rozvrh(chovatel){
    timetable=[];
    zoo.forEach(element =>{
        if(element.keeper===chovatel){
            timetable.push(element)
        }
    })
    return timetable;
}
console.log(rozvrh("Jan"))

// - bonus: spravte moznost zadat viac casov krmenia u zvierat
// - bonus 2: vytvorte funkciu, ktora vrati najblizsie dalsie krmenie pre osetrovatela podla aktualneho casu
function najblizsieKrmenie(chovatel){
    timetable = rozvrh(chovatel);
    date = new Date();
    hours = date.getHours();
    //minutes = date.getMinutes();
    let output = []
    output = timetable.filter(timetable => timetable.feeding>hours);
    let next = output[0];
    for(let i = 0;i<output.length;i++){
        if(next.feeding>output[i].feeding){
            next = output[i];
        }
    }
    return next;
}
console.log(najblizsieKrmenie("Jan"))
// - napr. getNextFeeding("Eva"), ked je 17:25 -> {​​​​​​​animal: "elephant", time: 18}​​​​​​​
